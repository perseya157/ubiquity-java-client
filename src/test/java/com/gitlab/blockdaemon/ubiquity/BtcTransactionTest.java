package com.gitlab.blockdaemon.ubiquity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import static com.gitlab.blockdaemon.ubiquity.TestUtil.readJsonFile;

import org.junit.BeforeClass;
import org.junit.Test;
import java.io.IOException;

import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.model.SignedTx;
import com.gitlab.blockdaemon.ubiquity.model.TxReceipt;
import com.gitlab.blockdaemon.ubiquity.tx.btc.Address;
import com.gitlab.blockdaemon.ubiquity.tx.btc.BtcTransaction;
import com.gitlab.blockdaemon.ubiquity.tx.btc.BtcService;
import com.gitlab.blockdaemon.ubiquity.tx.btc.BtcTransactionBuilder;
import com.gitlab.blockdaemon.ubiquity.tx.btc.KeyPair;
import com.gitlab.blockdaemon.ubiquity.tx.btc.PrivateKeyInfo;
import com.gitlab.blockdaemon.ubiquity.tx.btc.Script;
import com.gitlab.blockdaemon.ubiquity.tx.btc.UnspentOutputInfo;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class BtcTransactionTest {

	private static UbiquityClient client;
	private static MockWebServer mockBackEnd;
	private static String URL;

	@BeforeClass
	public static void setUp() throws IOException {	
		mockBackEnd = new MockWebServer();
		mockBackEnd.start();
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("btc_fee.json")).addHeader("Content-Type", "application/json"));
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("btc_tx_reciept.json")).addHeader("Content-Type", "application/json"));
		URL = String.format("http://%s:%s", mockBackEnd.getHostName(), mockBackEnd.getPort());
		client = new UbiquityClientBuilder().api(URL).build();
	}

	@Test
	public void shouldSendBtcTransaction() {
		String privateKey = "cTLFPEWG1CppcM7Q8DmrPNcr4TpqPxQgyVotCauQV2qNLNmPqvc2";
		String hashOfPrevTransaction = "d0921672675bd1b086ed959f8e6a81d0acacfc5c9853e2c34536a844f9bb66df";

		String changeAddress = "msb9xrrbDwnoofQ1VdinHRRg1zoPt42JHc";
		String outputAddress = "msEZfkqjgn423BmPc72d6i1fbeit2sCRLD";

		PrivateKeyInfo privateKeyInfo = PrivateKeyInfo.decode(privateKey);
		KeyPair keyPair = new KeyPair(privateKeyInfo, Address.Type.PUBLIC_KEY_TO_ADDRESS_LEGACY);

		UnspentOutputInfo unspentOutput = new UnspentOutputInfo()
				.keys(keyPair)
				.txHash(hashOfPrevTransaction)
				.value(1462631l)
				.outputIndex(0)
				.build();

		String suggestedFee = client.transactions().estimateFee(Platform.BITCOIN, Network.TEST_NET);

		SignedTx signedTx = new BtcTransactionBuilder()
				.fee(suggestedFee)
				.changeAddress(changeAddress)
				.outputAddress(outputAddress)
				.amountToSend(0)
				.addUnspentOutput(unspentOutput)
				.build();
		
		BtcTransaction tx = new BtcTransaction(BtcService.fromHex(signedTx.getTx()));
		assertThat(tx.getOutputs()[0].scriptPubKey.toString(), equalTo("OP_DUP OP_HASH160 8087f78926c74eae56a78dd8612317bbe18c841c OP_EQUALVERIFY OP_CHECKSIG"));

		TxReceipt receipt = client.transactions().txSend(Platform.BITCOIN, Network.TEST_NET, signedTx);
		assertThat(receipt.getId(), equalTo("8d225582cfdd7b3bd2586f66968e42db57d51ffdd059e746e3151ff637f6a93d"));
	}

}
