package com.gitlab.blockdaemon.ubiquity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import static com.gitlab.blockdaemon.ubiquity.TestUtil.readJsonFile;

import org.junit.BeforeClass;
import org.junit.Test;
import org.web3j.abi.FunctionEncoder;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionDecoder;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.gitlab.blockdaemon.ubiquity.tx.eth.EthTransactionBuilder;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.model.SignedTx;
import com.gitlab.blockdaemon.ubiquity.model.TxReceipt;
import com.gitlab.blockdaemon.ubiquity.tx.btc.Address;
import com.gitlab.blockdaemon.ubiquity.tx.btc.BtcTransaction;
import com.gitlab.blockdaemon.ubiquity.tx.btc.BtcService;
import com.gitlab.blockdaemon.ubiquity.tx.btc.BtcTransactionBuilder;
import com.gitlab.blockdaemon.ubiquity.tx.btc.KeyPair;
import com.gitlab.blockdaemon.ubiquity.tx.btc.PrivateKeyInfo;
import com.gitlab.blockdaemon.ubiquity.tx.btc.Script;
import com.gitlab.blockdaemon.ubiquity.tx.btc.UnspentOutputInfo;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class EthTransactionTest {

	private static UbiquityClient client;
	private static MockWebServer mockBackEnd;
	private static String URL = "https://stg.ubiquity.api.blockdaemon.com/v2";

	@BeforeClass
	public static void setUp() throws IOException {
		mockBackEnd = new MockWebServer();
		mockBackEnd.start();
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("eth_fee.json")).addHeader("Content-Type", "application/json"));
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("eth_tx_reciept.json")).addHeader("Content-Type", "application/json"));
		URL = String.format("http://%s:%s", mockBackEnd.getHostName(), mockBackEnd.getPort());
		client = new UbiquityClientBuilder().api(URL).build();
	}

	@Test
	public void shouldSendEthTransaction() {
		String fromPrivateKey = "91d75b8d4411a4cce7be104f3b4c149e583f1c2a6fdea78ad3498597ae997ed3";
		Credentials credentials = Credentials.create(fromPrivateKey);

        String toPublicKey ="0x32Ceb3C8409742A446c012Ed94DFa5672c28692B";
        BigInteger value = Convert.toWei("1.0", Convert.Unit.ETHER).toBigInteger();
		BigInteger nonce = BigInteger.valueOf(2);

		String suggestedFee = client.transactions().estimateFee(Platform.ETHEREUM, Network.TEST_NET);
		BigInteger gasprice = new BigInteger(suggestedFee);
		BigInteger gaslimit = BigInteger.valueOf(41000);

		// To prevent replay attacks the chain id should also be included https://chainid.network/
		int chainId = 3;

		RawTransaction rawTransaction = RawTransaction.createEtherTransaction(nonce, gasprice, gaslimit, toPublicKey, value);

		SignedTx tx = new EthTransactionBuilder().chainId(chainId).credentials(credentials).rawTransaction(rawTransaction).build();

		RawTransaction decodedTransaction = TransactionDecoder.decode(tx.getTx());
		assertThat(decodedTransaction.getGasPrice(), equalTo( new BigInteger("1000")));
		assertThat(decodedTransaction.getTo(), equalTo("0x32ceb3c8409742a446c012ed94dfa5672c28692b"));

		TxReceipt receipt = client.transactions().txSend(Platform.ETHEREUM, Network.TEST_NET, tx);
		assertThat(receipt.getId(), equalTo("0xe85bbf46dddeded571cc5c1df60a0df56029f86b7d8bcd1db79ebc25a359f3bb"));
	}

}
