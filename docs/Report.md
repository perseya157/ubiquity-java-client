

# Report


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**List&lt;ReportField&gt;**](ReportField.md) | Transaction items | 
**items** | **Integer** | The number of transactions in the report | 



