

# MultiTransfer

Transfer of currency in the UTXO model

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**List&lt;Utxo&gt;**](Utxo.md) |  | 
**outputs** | [**List&lt;Utxo&gt;**](Utxo.md) |  | 
**currency** | [**Currency**](Currency.md) |  | 
**totalIn** | **String** | Integer string in smallest unit (Satoshis) | 
**totalOut** | **String** | Integer string in smallest unit (Satoshis) | 
**unspent** | **String** | Integer string in smallest unit (Satoshis) | 



