

# TxPage


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** | Number of items in txs |  [optional]
**items** | [**List&lt;Tx&gt;**](Tx.md) |  |  [optional]
**continuation** | **String** | Token to get the next page |  [optional]



