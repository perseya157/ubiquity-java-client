

# Error


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | HTTP error type |  [optional]
**code** | **Integer** | Numeric error code |  [optional]
**title** | **String** | Short error description |  [optional]
**status** | **Integer** | HTTP status of the error |  [optional]
**detail** | **String** | Long error description |  [optional]



